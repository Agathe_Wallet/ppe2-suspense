# PPE2 TAF
## Projet de semestre 2 M1 TAL 2023.

**Objectif du projet** : repérer les thèmes, sujets et expressions qui ont fait l'actualité au fil de l'année 2022 dans les publications de Le Monde.
Les flux RSS nous ont été mis à disposition.

#### Etudiants : 
- Agathe Wallet
- Florian Jacquot
- Tifanny NGUYEN

#### Explication rapide des branches :
- La branche **main** est la branche principale du projet, c'est avec celle-ci que vous devez tester les scripts, si besoin.
  - Tous les scripts finaux et nécessaires ont été merge dessus.
- La branche **page** contient nos journaux de bords rassemblés par séances.
- Les autres branches sont des branches individuelles, nommées avec : nos initiales, _p_ si c'est la branche contenant notre journal de bord, et le numéro de la séance.

Nous avons opté pour l'option d'un rapport en .md pour privilégier le développement de nos scripts : **[lien vers le rapport.](https://gitlab.com/Agathe_Wallet/ppe2-TAF/-/blob/page/rapport/rapport.md "Rapport")**
