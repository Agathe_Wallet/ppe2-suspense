#!/usr/bin/env python3
# -*- coding: utf-8 -*-


"""

$ python3 ./scripts/run_lda.py ./output/output_dec_une.xml -l --pos NOUN VERB -o output/output_dec_une_n_v.html

"""

import logging, argparse, sys, re
from pprint import pprint
from datastructures import Token
from pathlib import Path
import xml.etree.ElementTree as ET
import pickle
import json

from gensim.models import Phrases
from gensim.corpora import Dictionary
from gensim.models import LdaModel

import pyLDAvis
import pyLDAvis.gensim_models as gensimvis

def log():
    return logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

def extract_xml(corpus_path:str):
	tree=ET.parse(corpus_path)
	root=tree.getroot()
	analyses = root.findall(".//analyse")

	docs=[]
	for analyse in analyses:
		article=[]
		for token in analyse.findall('token'):
			form = token.find('form').text
			lemma = token.find('lemma').text
			pos = token.find('pos').text
			dep = token.find('dep').text
			head = token.find('head').text
			token=Token(form, lemma, pos, dep, head)
			article.append(token)
		docs.append(article)
	return docs

def extract_pickle(corpus_path:str):
    # Load the pickled data
    with open(corpus_path, 'rb') as f:
        data = pickle.load(f)
    
    # Extract token information from data
    docs=[]
    for article in data:
        tokens=[]
        for token_info in article[4]:
            form = token_info[0]
            lemma = token_info[1]
            pos = token_info[2]
            dep = token_info[3]
            head = token_info[4]
            token=Token(form, lemma, pos, dep, head)
            tokens.append(token)
        docs.append(tokens)
    return docs

def extract_json(corpus_path:str):
    with open(corpus_path, 'rb') as f:
        data_str = f.read()
    data = json.loads(data_str)

    docs = []
    for date in data:
        for categorie in data[date]:
            for article in data[date][categorie]:
                tokens = []
                for token in article["analyse"]:
                    form = token["forme"]
                    lemma = token["lemme"]
                    pos = token["pos"]
                    dep = token["dep"]
                    head = token["head"]
                    token = Token(form, lemma, pos, dep, head)
                    tokens.append(token)
                docs.append(tokens)
    return docs


def numbers(docs : list):
    # Remove numbers, but not words that contain numbers.
    docs = [[token for token in doc if not token.isnumeric()] for doc in docs]
    return docs


def bigrams(docs : list):
    # Add bigrams and trigrams to docs (only ones that appear 20 times or more).
    bigram = Phrases(docs, min_count=20)
    for idx in range(len(docs)):
        for token in bigram[docs[idx]]:
            if '_' in token:
                # Token is a bigram, add to document.
                docs[idx].append(token)
    return docs

def get_top_topics(docs: list, num_topics:int, chunksize:int, passes:int, iterations:int, eval_every, no_below, no_above):

    dictionary = Dictionary(docs)
    dictionary.filter_extremes(no_below=no_below, no_above=no_above)
    corpus = [dictionary.doc2bow(doc) for doc in docs]

    # Make an index to word dictionary.
    temp = dictionary[0]  # This is only to "load" the dictionary.
    id2word = dictionary.id2token

    model = LdaModel(
        corpus=corpus,
        id2word=id2word,
        chunksize=chunksize,
        alpha='auto',
        eta='auto',
        iterations=iterations,
        num_topics=num_topics,
        passes=passes,
        eval_every=eval_every
    )

    top_topics = model.top_topics(corpus)
    return top_topics, model, corpus, dictionary

def get_avg_topic_coherence(top_topics, num_topics:int):
    # Average topic coherence is the sum of topic coherences of all topics, divided by the number of topics.
    avg_topic_coherence = sum([t[1] for t in top_topics]) / num_topics
    return avg_topic_coherence

def save_html_viz(model, corpus, dictionary, output_path):
    # ATTENTION, nécessite pandas en version 1.x
    vis_data = gensimvis.prepare(model, corpus, dictionary)
    with open(output_path, "w") as f:
        pyLDAvis.save_html(vis_data, f)



if __name__=='__main__':
    # log()
    
    parser = argparse.ArgumentParser()
    parser.add_argument("path", help="fichier d'entrée xml, json ou pickle")
    parser.add_argument("-l", "--lemma", nargs="*", help="utiliser les lemmes pour le topic modeling")
    parser.add_argument("-f", "--form", nargs="*", help="utiliser les formes pour le topic modeling")
    parser.add_argument("--pos", nargs="*", help="permet de filtrer par pos")
    parser.add_argument("-nb", nargs="*", default= 20, help="exclude tokens that appear in less than no_below documents")
    parser.add_argument("-na", type=float, help="exclude tokens that appear in more than no_above proportion of documents")
    parser.add_argument("-nt", nargs="*", help="nombre de topics")
    parser.add_argument("-cs", nargs="*", help="chunksize")
    parser.add_argument("-p", nargs="*", help="passes")
    parser.add_argument("-i", nargs="*", help="iterations")
    parser.add_argument("-o", default=None, help="génère la visualisation ldaviz et la sauvegarde dans le fichier html indiqué")
    args = parser.parse_args()
    
    if ".xml" in args.path:
        corpus=extract_xml(args.path)
    elif ".json" in args.path:
        corpus=extract_json(args.path)
    elif ".pickle" in args.path or ".pkl" in args.path:
        corpus=extract_pickle(args.path)
    
    
    if '-nb' in sys.argv:
        index_nb = sys.argv.index('-nb')
        no_below = int(sys.argv[index_nb +1])
    else:
        no_below=20
    if '-na' in sys.argv:
        index_na = sys.argv.index('-na')
        no_above = float(sys.argv[index_na + 1])
    else:
        no_above=0.5
    if '-nt' in sys.argv:
        num_topics=int(sys.argv[sys.argv.index('-nt') + 1])
    else:
        num_topics = 10
    if '-cs' in sys.argv:
        chunksize=int(sys.argv[sys.argv.index('-cs')+1])
    else:
        chunksize = 2000
    if '-p' in sys.argv:
        passes=int(sys.argv[sys.argv.index('-p')+1])
    else:
        passes = 20
    if '-i' in sys.argv:
        iterations=int(sys.argv[sys.argv.index('-i')+1])
    else:
        iterations = 400


    docs=[]
    for art in corpus:
        if '--pos' in sys.argv:
            if "-l" in sys.argv or "--lemma" in sys.argv:
                doc=[token.lemme for token in art if token.pos in args.pos]
            elif "-f" in sys.argv or "--form" in sys.argv:
                doc=[token.forme for token in art if token.pos in args.pos]
        else:
            if "-l" in sys.argv or "--lemma" in sys.argv:
                doc=[token.lemme for token in art]
            elif "-f" in sys.argv or "--form" in sys.argv:
                doc=[token.forme for token in art]
        docs.append(doc)

    
    
    docs=bigrams(numbers(docs))
    eval_every = None  # Don't evaluate model perplexity, takes too much time.
    topics, model, cor, dic =get_top_topics(docs, num_topics, chunksize, passes, iterations, eval_every, no_below, no_above)
    avg_coherence=get_avg_topic_coherence(topics, num_topics)   

    # pprint(topics)
    # print(avg_coherence)

    if args.o is not None:
        save_html_viz(model, cor, dic, args.o)

    

