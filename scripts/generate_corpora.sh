#!/bin/bash

######################################################################################
#
#
####### Exécution du script à partir du répertoire principal: 
#
# 
####### $ ./scripts/generate_corpora.sh -i <input/corpus/path> -o <output/path/file_basename> -p <pos1> -p <pos2> -c <cat1> -c <cat2> -f <ext1> -f <ext2> -s <2022-mm-jj> -e <2022-mm-jj> -t <lemma|form> -B <no_below> -A <no_above> -T <num_topics> -C chunksize -P <passes> -I <iterations> -m|-a -c
#
#
####### Options obligatoires : -i -o -f -t
#
#
####### Options sans argument : 
### -a : parsing sur l'année
### -m : parsing par mois
### -g : parsing par catégorie
#
#
######################################################################################

par_mois=false
par_annee=false
par_cat=false

while getopts "ami:o:p:c:f:s:e:t:B:A:T:C:P:I:g" option; do
  case $option in
    a)
    par_annee=true
    ;;
    m)
    par_mois=true
    ;;
    i) 
    corpus_path="$OPTARG"
    ;;
    o) 
    output_path="$OPTARG"
    ;;
    p) 
    pos+=("$OPTARG")
    ;;
    c) 
    cat+=("$OPTARG")
    ;;
    f) 
    formats+=("$OPTARG")
    ;;
    s)
    start_date="$OPTARG"
    ;;
    e)
    end_date="$OPTARG"
    ;;
    t)
    token_type="$OPTARG"
    ;;
    B)
    no_below="$OPTARG"
    ;;
    A)
    no_above="$OPTARG"
    ;;
    T)
    num_topics="$OPTARG"
    ;;
    C)
    chunksize="$OPTARG"
    ;;
    P)
    passes="$OPTARG"
    ;;
    I)
    iterations="$OPTARG"
    ;;
    g)
    par_cat=true
    ;;
  esac
done

# Si on ne précise pas si on veut un fichier par mois ou un fichier pour l'année entière, on prend l'année entière par défaut
if [ $par_mois == false ] && [ $par_annee == false ] && [ -z $start_date ] && [ -z $end_date ]; then
    par_annee=true
fi


# Création de variable d'extension de sortie, en fonction du nb d'extensions déclarées (max 3 -> json, xml et pickle)
if [ ${#formats[@]} == 1 ]; then
    output_ext1=${formats[0]}
elif [ ${#formats[@]} == 2 ]; then
    output_ext1=${formats[0]}
    output_ext2=${formats[1]}
elif [ ${#formats[@]} == 3 ]; then
    output_ext1=${formats[0]}
    output_ext2=${formats[1]}
    output_ext3=${formats[2]}
elif [ -z "$format" ]; then
    output1=$"pickle"
fi

# Si des catégories ont été précisées
if [ -n "$cat" ]; then
     # Récupère la liste des cat demandées
     cat_n=$""
     for c in ${cat[@]}; do
         cat_n=$"$cat_n"_"$c"
     done
fi

# Si des pos ont été précisées
pos_n=$""
if [ -n "$pos" ]; then
    # Récupère la liste des POS demandées
    for p in ${pos[@]}; do
        pos_n=$"$pos_n"_"$p"
    done
fi

# Création d'une chaîne de caractères pour regrouper toutes les options déclarées pour l'exécution du script run_lda.py
options_lda=$""
if [ -n "$no_below" ]; then
    options_lda=$"$options_lda -nb $no_below"
fi
if [ -n "$no_above" ]; then
    options_lda=$"$options_lda -na $no_above"
fi
if [ -n "$num_topics" ]; then
    options_lda=$"$options_lda -nt $num_topics"
fi
if [ -n "$chunksize" ]; then
    options_lda=$"$options_lda -cs $chunksize"
fi
if [ -n "$passes" ]; then
    options_lda=$"$options_lda -p $passes"
fi
if [ -n "$iterations" ]; then
    options_lda=$"$options_lda -i $iterations"
fi

mois=("jan" "feb" "mar" "apr" "may" "jun" "jul" "aug" "sep" "oct" "nov" "dec")

cat_list=("une" "international" "europe" "societe" "idees" "economie" "actualite-medias" "sport" "planete" "culture" "livres" "cinema" "voyage" "technologies" "politique" "sciences")

# pos_list=("ADJ" "ADP" "ADV" "AUX" "CONJ" "CCONJ" "DET" "INTJ" "NOUN" "NUM" "PART" "PRON" "PROPN" "PUNCT" "SCONJ" "SYM" "VERB" "X" "SPACE")

# Si pas de période prédéfinie et parsing par mois
if [ -z "$start_date" ] && [ -z "$end_date" ] ; then
    if $par_mois; then
        # Génère pour chaque mois un fichier pickle, json et/ou xml
        for i in {0..11}; do
            # Récupère le mois dans la liste pour nom fichier
            mois_str=${mois[i]}

            # Donne le numéro du mois sous format 1 digit pour mois 1 à 9
            mois_1d=$((i + 1))
            
            # Donne le numéro du mois sous format 2 digit
            mois_2d=$(printf "%02d" "$mois_1d")
            
            # Récupère le dernier jour du mois en cours de traitement
            last_day=$(date -d "2022-$mois_2d-01 +1 month -1 day" +%d)
            
            if [ $par_cat == true ]; then
                for c in "${cat_list[@]}"; do
                    if [ ! -v output_ext2 ]; then
                        output1=$"$output_path"_"$mois_str"_"$c".$output_ext1
                        python3 scripts/extraire_deux.py "$corpus_path" "$c" -o $output1
                        
                        # S'il y a deux formats de sortie
                    elif [ ! -v output_ext3 ]; then
                        output1=$"$output_path"_"$mois_str"_"$c".$output_ext1
                        output2=$"$output_path"_"$mois_str"_"$c".$output_ext2
                        python3 scripts/extraire_deux.py "$corpus_path" "$c" -o $output1 $output2
                        
                        # S'il y a trois formats de sortie
                    else
                        output1=$"$output_path"_"$mois_str"_"$c".$output_ext1
                        output2=$"$output_path"_"$mois_str"_"$c".$output_ext2
                        output3=$"$output_path"_"$mois_str"_"$c".$output_ext3
                        python3 scripts/extraire_deux.py "$corpus_path" "$c" -o $output1 $output2 $output3
                    fi
                    
                    # Génère analyse LDA à partir du fichier pickle/xml/json (si plusieurs formats de sortie précédemment sélectionné, prend le premier)
                    if [ -n "$pos" ]; then
                        output_html=$"$output_path"_"$mois_str"_"$c"_"$token_type""$pos_n".html
                        if [ $token_type == "lemma" ] ; then
                            python3 ./scripts/run_lda.py $output1 -l --pos "${pos[@]}" "$options_lda" -o $output_html
                        elif [ $token_type == "form" ] ; then
                            python3 ./scripts/run_lda.py $output1 -f --pos "${pos[@]}" "$options_lda" -o $output_html
                        fi
                    else
                        output_html=$"$output_path"_"$mois_str"_"$c"_"$token_type".html
                        if [ $token_type == "lemma" ] ; then
                            python3 ./scripts/run_lda.py $output1 -l "$options_lda" -o $output_html
                        elif [ $token_type == "form" ] ; then
                            python3 ./scripts/run_lda.py $output1 -f "$options_lda" -o $output_html
                        fi
                    fi
                done
            else
                # S'il n'y a qu'un seul format de sortie
                if [ ! -v output_ext2 ]; then
                    output1=$"$output_path"_"$mois_str""$cat_n".$output_ext1
                    python3 ./scripts/extraire_deux.py "$corpus_path" "${cat[@]}" -s "2022-$mois_2d-01" -e "2022-$mois_2d-$last_day" -o $output1
                
                # S'il y a deux formats de sortie
                elif [ ! -v output_ext3 ]; then
                    output1=$"$output_path"_"$mois_str""$cat_n".$output_ext1
                    output2=$"$output_path"_"$mois_str""$cat_n".$output_ext2
                    python3 ./scripts/extraire_deux.py "$corpus_path" "${cat[@]}" -s "2022-$mois_2d-01" -e "2022-$mois_2d-$last_day" -o $output1 $output2
                
                # S'il y a trois formats de sortie
                else
                    output1=$"$output_path"_"$mois_str""$cat_n".$output_ext1
                    output2=$"$output_path"_"$mois_str""$cat_n".$output_ext2
                    output3=$"$output_path"_"$mois_str""$cat_n".$output_ext3
                    python3 ./scripts/extraire_deux.py "$corpus_path" "${cat[@]}" -s "2022-$mois_2d-01" -e "2022-$mois_2d-$last_day" -o $output1 $output2 $output3
                fi
                # Génère analyse LDA à partir du fichier pickle/xml/json (si plusieurs formats de sortie précédemment sélectionné, prend le premier)
                output_html=$"$output_path"_"$mois_str""$cat_n"_"$token_type""$pos_n".html
                if [ -n "$pos" ]; then
                    if [ $token_type == "lemma" ] ; then
                        python3 ./scripts/run_lda.py $output1 -l --pos "${pos[@]}" "$options_lda" -o $output_html
                    elif [ $token_type == "form" ] ; then
                        python3 ./scripts/run_lda.py $output1 -f --pos "${pos[@]}" "$options_lda" -o $output_html
                    fi
                else
                    if [ $token_type == "lemma" ] ; then
                        python3 ./scripts/run_lda.py $output1 -l "$options_lda" -o $output_html
                    elif [ $token_type == "form" ] ; then
                        python3 ./scripts/run_lda.py $output1 -f "$options_lda" -o $output_html
                    fi
                fi
            fi
        done
    elif $par_annee; then
        if [ $par_cat == true ]; then
            for c in "${cat_list[@]}"; do
                if [ ! -v output_ext2 ]; then
                    output1=$"$output_path"_"2022"_"$c".$output_ext1
                    python3 scripts/extraire_deux.py "$corpus_path" "$c" -o $output1
                
                # S'il y a deux formats de sortie
                elif [ ! -v output_ext3 ]; then
                    output1=$"$output_path"_"2022"_"$c".$output_ext1
                    output2=$"$output_path"_"2022"_"$c".$output_ext2
                    python3 scripts/extraire_deux.py "$corpus_path" "$c" -o $output1 $output2
                
                # S'il y a trois formats de sortie
                else
                    output1=$"$output_path"_"2022"_"$c".$output_ext1
                    output2=$"$output_path"_"2022"_"$c".$output_ext2
                    output3=$"$output_path"_"2022"_"$c".$output_ext3
                    python3 scripts/extraire_deux.py "$corpus_path" "$c" -o $output1 $output2 $output3
                fi
                output_html=$"$output_path"_"2022"_"$c"_"$token_type""$pos_n".html
                # Génère analyse LDA à partir du fichier pickle/xml/json (si plusieurs formats de sortie précédemment sélectionné, prend le premier)
                if [ -n "$pos" ]; then
                    if [ $token_type == "lemma" ] ; then
                        python3 ./scripts/run_lda.py $output1 -l --pos "${pos[@]}" "$options_lda" -o $output_html
                    elif [ $token_type == "form" ] ; then
                        python3 ./scripts/run_lda.py $output1 -f --pos "${pos[@]}" "$options_lda" -o $output_html
                    fi
                else
                    if [ $token_type == "lemma" ] ; then
                        python3 ./scripts/run_lda.py $output1 -l "$options_lda" -o $output_html
                    elif [ $token_type == "form" ] ; then
                        python3 ./scripts/run_lda.py $output1 -f "$options_lda" -o $output_html
                    fi
                fi
            done
        else 
            # S'il n'y a qu'un format de sortie
            if [ ! -v output_ext2 ]; then
                output1=$"$output_path"_"2022""$cat_n".$output_ext1
                python3 scripts/extraire_deux.py "$corpus_path" "${cat[@]}" -o $output1
            
            # S'il y a deux formats de sortie
            elif [ ! -v output_ext3 ]; then
                output1=$"$output_path"_"2022""$cat_n".$output_ext1
                output2=$"$output_path"_"2022""$cat_n".$output_ext2
                python3 scripts/extraire_deux.py "$corpus_path" "${cat[@]}" -o $output1 $output2
            
            # S'il y a trois formats de sortie
            else
                output1=$"$output_path"_"2022""$cat_n".$output_ext1
                output2=$"$output_path"_"2022""$cat_n".$output_ext2
                output3=$"$output_path"_"2022""$cat_n".$output_ext3
                python3 scripts/extraire_deux.py "$corpus_path" "${cat[@]}" -o $output1 $output2 $output3
            fi
            output_html=$"$output_path"_"2022""$cat_n"_"$token_type""$pos_n".html
            if [ -n "$pos" ]; then
                if [ $token_type == "lemma" ] ; then
                    python3 ./scripts/run_lda.py $output1 -l --pos "${pos[@]}" "$options_lda" -o $output_html
                elif [ $token_type == "form" ] ; then
                    python3 ./scripts/run_lda.py $output1 -f --pos "${pos[@]}" "$options_lda" -o $output_html
                fi
            else
                if [ $token_type == "lemma" ] ; then
                    python3 ./scripts/run_lda.py $output1 -l "$options_lda" -o $output_html
                elif [ $token_type == "form" ] ; then
                    python3 ./scripts/run_lda.py $output1 -f "$options_lda" -o $output_html
                fi
            fi
        fi
    fi

# Période prédéfinie 
elif [ -n "$start_date" ] || [ -n "$end_date" ] ; then
    if [ -z "$start_date" ]; then
        start_date=$"2022-01-01"
    fi
    if [ -z "$end_date" ]; then
        end_date=$"2022-12-31"
    fi
    echo "start date : $start_date"
    echo "end date : $end_date"
    if [ $par_cat == true ]; then
        for c in "${cat_list[@]}"; do
            # S'il n'y a qu'un format de sortie
            if [ ! -v output_ext2 ]; then
                output1=$"$output_path"_"$start_date"_"$end_date"_"$c".$output_ext1
                python3 scripts/extraire_deux.py "$corpus_path" "$c" -s $start_date -e $end_date -o $output1

            
            # S'il y a deux formats de sortie
            elif [ ! -v output_ext3 ]; then
                output1=$"$output_path"_"$start_date"_"$end_date"_"$c".$output_ext1
                output2=$"$output_path"_"$start_date"_"$end_date"_"$c".$output_ext2
                python3 scripts/extraire_deux.py "$corpus_path" "$c" -s $start_date -e $end_date -o $output1 $output2
            
            # S'il y a trois formats de sortie
            else
                output1=$"$output_path"_"$start_date"_"$end_date"_"$c".$output_ext1
                output2=$"$output_path"_"$start_date"_"$end_date"_"$c".$output_ext2
                output3=$"$output_path"_"$start_date"_"$end_date"_"$c".$output_ext3
                python3 scripts/extraire_deux.py "$corpus_path" "$c" -s $start_date -e $end_date -o $output1 $output2 $output3
            fi
            # Génère analyse LDA à partir du fichier pickle/xml/json (si plusieurs formats de sortie précédemment sélectionné, prend le premier)
            output_html=$"$output_path"_"$start_date"_"$end_date"_"$c"_"$token_type""$pos_n".html
            if [ -n "$pos" ]; then
                if [ $token_type == "lemma" ] ; then
                    python3 scripts/run_lda.py $output1 -l --pos "${pos[@]}" $options_lda -o $output_html
                elif [ $token_type == "form" ] ; then
                    python3 scripts/run_lda.py $output1 -f --pos "${pos[@]}" $options_lda -o $output_html
                fi 
            else
                if [ $token_type == "lemma" ] ; then
                    python3 scripts/run_lda.py $output1 -l $options_lda -o $output_html
                elif [ $token_type == "form" ] ; then
                    python3 scripts/run_lda.py $output1 -f $options_lda -o $output_html
                fi 
            fi
        done
    else

        # S'il n'y a qu'un format de sortie
        if [ ! -v output_ext2 ]; then
            output1=$"$output_path"_"$start_date"_"$end_date""$cat_n".$output_ext1
            python3 scripts/extraire_deux.py "$corpus_path" "${cat[@]}" -s $start_date -e $end_date -o $output1
            
        # S'il y a deux formats de sortie
        elif [ ! -v output_ext3 ]; then
            output1=$"$output_path"_"$start_date"_"$end_date""$cat_n".$output_ext1
            output2=$"$output_path"_"$start_date"_"$end_date""$cat_n".$output_ext2
            python3 scripts/extraire_deux.py "$corpus_path" "${cat[@]}" -s $start_date -e $end_date -o $output1 $output2
        
        # S'il y a trois formats de sortie
        else
            output1=$"$output_path"_"$start_date"_"$end_date""$cat_n".$output_ext1
            output2=$"$output_path"_"$start_date"_"$end_date""$cat_n".$output_ext2
            output3=$"$output_path"_"$start_date"_"$end_date""$cat_n".$output_ext3
            python3 scripts/extraire_deux.py "$corpus_path" "${cat[@]}" -s $start_date -e $end_date -o $output1 $output2 $output3
        fi
        # Génère analyse LDA à partir du fichier pickle/xml/json (si plusieurs formats de sortie précédemment sélectionné, prend le premier)
        output_html=$"$output_path"_"$start_date"_"$end_date""$cat_n"_"$token_type""$pos_n".html
        if [ -n "$pos" ]; then
            if [ $token_type == "lemma" ] ; then
                python3 scripts/run_lda.py $output1 -l --pos "${pos[@]}" $options_lda -o $output_html
            elif [ $token_type == "form" ] ; then
                python3 scripts/run_lda.py $output1 -f --pos "${pos[@]}" $options_lda -o $output_html
            fi 
        else
            if [ $token_type == "lemma" ] ; then
                python3 scripts/run_lda.py $output1 -l $options_lda -o $output_html
            elif [ $token_type == "form" ] ; then
                python3 scripts/run_lda.py $output1 -f $options_lda -o $output_html
            fi 
        fi
    fi
fi


