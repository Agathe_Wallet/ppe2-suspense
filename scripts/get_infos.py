#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Created on Fri Apr  7 09:34:59 2023

@author: agathew
"""

def get_date(file_path):
    coupe_path=file_path.split('/')
    j=coupe_path[-3]
    m=coupe_path[-4]
    y=coupe_path[-5]
    return f"{j} {m} {y}"

def get_cat(file_path, dico_cat):
    coupe_path=file_path.split('/')
    coupe_ext=coupe_path[-1].split('.')
    for cle, valeur in dico_cat.items():
        if valeur == coupe_ext[0]:
            return cle 

def conv_mois(liste_mois, mois_str):
    return (liste_mois.index(mois_str)+1)

