#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pickle, argparse
import pprint as pp

parser = argparse.ArgumentParser()
parser.add_argument("path", help="path of the file to read")
args = parser.parse_args()
path=args.path

objects = []
with (open(path, "rb")) as openfile:
    while True:
        try:
            objects.append(pickle.load(openfile))
        except EOFError:
            break

pp.pprint(objects, sort_dicts=False)