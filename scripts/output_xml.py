#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  7 09:32:52 2023

@author: agathew
"""
from datastructures import Article, Corpus, Token
import extraire_un as e1
from lxml import etree



def article_to_xml(article: Article):
    art=etree.Element("article")
    title=etree.SubElement(art, "title")
    title.text=article.titre
    desc=etree.SubElement(art, "description")
    desc.text=article.description
    analyse=etree.SubElement(art, "analyse")
    analyse.text="\n		  "
    for i in range(len(article.analyse)):
        token=article.analyse[i]
        tok = etree.SubElement(analyse, "token")
        tok.set("id", str(i))
        form=etree.SubElement(tok, "form")
        form.text = token.forme
        lemma=etree.SubElement(tok, "lemma")
        lemma.text = token.lemme
        pos=etree.SubElement(tok, "pos")
        pos.text = token.pos
        dep=etree.SubElement(tok, "dep")
        dep.text = token.dep
        head=etree.SubElement(tok, "head")
        head.text = token.head
        if i < (len(article.analyse)-1):
            tok.tail="\n		  "
        else:
            tok.tail="\n        "
        
    return art


def output_xml(corpus: Corpus):
    root = etree.Element("feed")
    root.set("begin", str(corpus.begin))
    root.set("end", str(corpus.end))
    date_cat_art = {}
    for article in corpus.articles:
        if article.date not in date_cat_art:
            date_cat_art[article.date] = {}
        if article.categorie not in date_cat_art[article.date]:
            date_cat_art[article.date][article.categorie] = etree.SubElement(root, "categorie")
            date_cat_art[article.date][article.categorie].set("label", article.categorie)
        date_cat_art[article.date][article.categorie].append(article_to_xml(article))


    for date, categories in date_cat_art.items():
        date_elem = etree.SubElement(root, "date")
        date_elem.set("d", date)
        for category, articles in categories.items():
            date_elem.append(articles)
          
    return etree.tostring(root, pretty_print=True, xml_declaration=True, encoding="utf-8").decode()



