#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
import pathlib
import argparse
from get_infos import get_date, get_cat
from datastructures import Article

regex_item = re.compile("<item><title>(.*?)<\/title>.*?<description>(.*?)<\/description>")


def nettoyage(texte):
    texte_net = re.sub("<!\[CDATA\[(.*?)\]\]>", "\\1", texte) 
    return texte_net

def extract_re(file_path, dico_cat):
    date=get_date(str(file_path))
    cat=get_cat(str(file_path), dico_cat)

    f=pathlib.Path(file_path)                                   #ne lit pas ligne par ligne contrairement à open(file,"r")
    content=f.read_text()
    for m in re.finditer(regex_item, content):
        title = nettoyage(m.group(1))
        desc = nettoyage(m.group(2))
        yield Article(date, cat, title, desc, [])

def main(path, extract_func):
    title_list, desc_list=extract_func(path)
    print(f"Liste des titres :\n{title_list}\n\nListe des catégories :\n{desc_list}")
    
if __name__=='__main__':
    parseur = argparse.ArgumentParser(prog="ReadFiles", description='Read files from the corpus')
    parseur.add_argument("path", nargs='*', help="permet de donner les fichiers d'entrée en argument")
    args = parseur.parse_args()

    if args.path :
        path=args.path
    else : 
        print("Merci d'indiquer le nom du fichier pour lequel vous souhaitez exécuter ce programme.")

    main(path)

