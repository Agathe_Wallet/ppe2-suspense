#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  7 09:17:00 2023

@author: agathew
"""

from typing import List, Dict, Optional
from dataclasses import dataclass
from pathlib import Path

@dataclass
class Token:
    forme: str
    lemme: str
    pos: str
    dep: str
    head: Optional[str] = None
    
@dataclass
class Article:
    date: str
    categorie: str
    titre: str
    description: str
    analyse: List[Token]
    


@dataclass
class Corpus:
    categories: List[str]
    begin: str
    end: str
    chemin: Path
    articles: List[Article]
    topics: list
    avg_coherence: float
    



