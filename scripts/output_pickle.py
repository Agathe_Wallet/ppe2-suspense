#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Created on Mon Apr 10 2023

@author: tsngu

Script bordélique, désolée...mais il marche
"""


from datastructures import Article, Corpus, Token
from tagging import tag_article
import pprint as pp

def output_pickle(corpus: Corpus):
    output = []
    for article in corpus.articles:
        article_info = [str(article.date), article.categorie, article.titre, article.description]
        article_tokens = []
        for token in article.analyse:
            token_info = [token.forme, token.lemme, token.pos, token.dep, token.head]
            article_tokens.append(token_info)
        article_info.append(article_tokens)
        output.append(article_info)
    return output


